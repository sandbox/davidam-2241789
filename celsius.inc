<?php

/**
 * @file
 * This is the Celsius & Fahrenheit Module
 * 
 */

function celsius_form_upload($form, &$form_state) {

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('This module transform from celsius to fahrenheit (or inverse)'),
  );

  $form['fahrenheit'] = array(
    '#type' => 'textfield',
    '#title' => t('Fahrenheit'),
  );

  $form['celsius'] = array(
    '#type' => 'textfield',
    '#title' => t('Celsius'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;

}


/**
 * Form to upload org files celsius_form_settings_submit().
 */
function celsius_form_upload_submit($form, &$form_state) {

  //  drupal_set_message(t('The form has been submitted. The path is: '));
  // . $form["path"]["#value"]);
  $fahrenheit = $form_state['values']['fahrenheit'];
  $celsius = $form_state['values']['celsius'];
  if ($celsius != "") {
    $fahrenheitG = (9.0/5.0) * $celsius;
    drupal_set_message(t('Celsius: '). $celsius .  t(' Fahrenheit: '). $fahrenheitG);
    //Un cambio de 36 grados Celsius corresponde a 64.8 grados Farenheit.
  }

  if ($fahrenheit != "") {
    $celsiusG = $fahrenheit * (5.0/9.0);
    drupal_set_message(t(' Fahrenheit: ') . $fahrenheit . t(' Celsius: '). $celsiusG);
  }

}

/**
 * Validate handler for celsius_form_upload_validate().
 */

function celsius_form_upload_validate($form, &$form_state) {
  $fahrenheit = $form_state['values']['fahrenheit'];
  $celsius = $form_state['values']['celsius'];
  if (($fahrenheit != "") && ($celsius != "")) {
    form_set_error('celsius', t('You must introduce celsius or fahrenheit, but not both'));
  }
}
